import java.util.*;
public class ArrayStuffTask {
    public static void main(String[] args) {
        int[] arrayOfIntegers = {-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5};

        Arrays.sort(arrayOfIntegers);
        System.out.println(Arrays.toString(arrayOfIntegers));

        int left = 0;
        int right = arrayOfIntegers.length - 1;

        while (left < right)
        {
            int leftVal = arrayOfIntegers[left];
            int rightVal = (arrayOfIntegers[right]);
            int sum = leftVal + rightVal;

            if (sum == 3)
            {
                System.out.println(arrayOfIntegers[left] + "," + arrayOfIntegers[right]);
                right --;
                left++;
            }
            if (sum > 3)
            {
                right --;
            }
            if (sum < 3)
            {
                left++;
            }
        }
    }
}





